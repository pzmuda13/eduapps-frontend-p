var solver = require('javascript-lp-solver')

var next_id;

var init = function() {
  next_id = 2;
};

exports.init = init;

var newProblem = function(numOfVariables,numOfConstraints, maxSteps, minSteps) {

  while(true)
  {
    let goal;
    let performance_index = [];
    let constraints = [];
    let problem = {};

    if(Math.round(Math.random())) //Krok 1
    {
      goal='max';
    }
    else {
      goal='min';
    }

    for (var i=0; i<numOfVariables; i++) // Krok 2
    {
      performance_index.push(Math.floor(Math.random() * 9) + 1  )
    }

    for (var i=0; i<numOfConstraints; i++) // Krok 3
    {
      let constraint = new Array(numOfVariables).fill(0);
      constraint[Math.floor(Math.random() * numOfVariables)] =      // Krok 3a
        Math.floor(Math.random() * 9) + 1;                          // Krok 3b
      goal == 'min'? constraint.push('>=') : constraint.push('<=')  // Krok 3c
      constraint.push(Math.floor(Math.random() * 9) + 1);           // Krok 3d
      constraints.push(constraint);
    }

    for (var i=0; i<numOfVariables; i++)
    {
      constraints[Math.floor(Math.random() * numOfConstraints)][i] = // Krok 4a
        Math.floor(Math.random() * 9) + 1;                           // Krok 4b
    }

    problem = {
      id: 1,
      parent_id: null,
      performance_index: performance_index,
      goal: goal,
      constraints: constraints,
      solution: null,
      estimate: null,
      status: 'to-solve'
    }

    init();
    if(autoSolve(problem, maxSteps, minSteps))  // Krok 5
    {
      init();
      return problem;
    };
  }
};

exports.newProblem = newProblem;

var createModel = function( problem ) {
	var model = [];
	var variables = ``;

	for (var i = 0, len = problem.performance_index.length; i < len; i++) {
			var index = problem.performance_index[i];
			var number = i+1;

			variables = variables.concat(` ${index} x${number}`);
	}

	model.push(`${problem.goal}: ${variables}`);

	for (var i = 0, len = problem.constraints.length; i < len; i++) {

			var constLen = problem.constraints[i].length;
			var value = problem.constraints[i][constLen-1];
			var sign = problem.constraints[i][constLen-2];
			var variables = '';

			for (var j = 0; j < constLen-2; j++) {

					var index = problem.constraints[i][j];
					var number = j+1;
					if(index != 0)
					{
						variables = variables.concat(`${index} x${number} `);
					}

			}

			model.push(`${variables}${sign} ${value}`);
	}
	return model;
}

var createSubproblems = function( problem, checkConstraints ) {

	var problems = [];
  var newConstraints = [];
	var solution = problem.solution;
	var values = solution.variables;
  var answer = {problems: problems, constraints: newConstraints};

	if(solution.feasible)
	{
		for (var i = 0; i < values.length; i++) {
				var newProblem1 = JSON.parse(JSON.stringify(problem));
				var newProblem2 = JSON.parse(JSON.stringify(problem));
				var constraint1 = new Array(newProblem1.performance_index.length).fill(0);
				var constraint2 = new Array(newProblem2.performance_index.length).fill(0);


				if(values[i] != Math.floor(values[i]))
				{
					constraint1[i]=1;
					newProblem1.constraints.push(constraint1.concat(['<=',Math.floor(values[i])]));
          newConstraints.push(constraint1.concat(['<=',Math.floor(values[i])]));
					newProblem1.parent_id = newProblem1.id;
					if(!checkConstraints) newProblem1.id = next_id++;
					newProblem1.solution = null;
          newProblem1.estimate = null;

					constraint2[i]=1;
					newProblem2.constraints.push(constraint2.concat(['>=',Math.ceil(values[i])]));
          newConstraints.push(constraint2.concat(['>=',Math.ceil(values[i])]));
					newProblem2.parent_id = newProblem2.id;
					if(!checkConstraints) newProblem2.id = next_id++;
					newProblem2.solution = null;
          newProblem2.estimate = null;

					problems.push(newProblem1);
					problems.push(newProblem2);
				}
		}
	}

	return answer;
}

exports.createSubproblems = createSubproblems;

var generateProblemLabel = function( problem, estimation? ) {

  var label = '';
	var variables = ``;
	var constraints = [];

	for (var i = 0, len = problem.performance_index.length; i < len; i++) {
			var index = problem.performance_index[i];
			var number = i+1;
			if(index > 0)
			{
				if(variables.length != 0) variables = variables.concat('+')
				variables = variables.concat(`${index}*x${number}`)
			}
			else
			{
				variables = variables.concat(`${index}*x${number}`)
			}
	}

	var fun = `${problem.goal}: ${variables}\n`;

	for (var i = 0, len = problem.constraints.length; i < len; i++) {

			var constLen = problem.constraints[i].length;
			var value = problem.constraints[i][constLen-1];
			var sign = problem.constraints[i][constLen-2];
			var variables = '';

			for (var j = 0; j < constLen-2; j++) {

					var index = problem.constraints[i][j];
					var number = j+1;
					if(index > 0)
					{
						if(variables.length != 0) variables = variables.concat('+')
						variables = variables.concat(`${index}*x${number} `);
					}
					else if (index <0)
					{
						variables = variables.concat(`${index}*x${number} `);
					}

			}

			constraints.push(`\n${variables}${sign} ${value}`);
	}

  label = fun.concat(constraints);


  if (problem.solution)
  {
    let result;
    problem.solution.feasible? result = new Number(problem.solution.result).toFixed(2): result = "Brak"
    label = label.concat(`\n\nWynik RL: ${result}`);
  }


  if (estimation)
  {
    label = label.concat(`\nOszacowanie: ${estimation}`);
  }

	return label;
}

exports.generateProblemLabel = generateProblemLabel;

var setSolution = function( solution, problem)
{
		var variables = new Array(problem.performance_index.length).fill(0);

		for (const key of Object.keys(solution)) {
				const val = solution[key];

				if(key != 'feasible' && key != 'result')
				{
						variables[parseInt(key.slice(1))-1] = val;
				}
	 }

	 var formattedSolution = {
		 feasible: solution.feasible,
		 result: solution.result,
		 variables: variables
	 }
    var solved = Object.assign({}, problem);
	 solved.solution = formattedSolution;

   return solved;
}

var solve = function( problem ) {

	var model = solver.ReformatLP(createModel(problem));
	var solution = solver.Solve(model);

	return setSolution(solution, problem);
}

exports.solve = solve;

var autoSolve = function( startingProblem, maxSteps, minSteps ) {
    var problems = [Object.assign({}, startingProblem)];
    var solved = false;
    var steps = 0;
    var finalSolution;

    while(!solved && steps <= maxSteps)
    {
        let bestID = findBestProblem(problems);

        problems[bestID] = solve(problems[bestID])

        if(checkIfInts(problems[bestID]))
        {
          if(steps >= minSteps && problems[bestID].solution.result > 0)
          {
            solved = true;
            finalSolution = problems[bestID];
          }
          break;
        }
        else
        {
          var newProblems = createSubproblems(problems[bestID], false).problems;
          problems = problems.concat(newProblems);
          problems[bestID].status = 'not-important';
          steps++;
        }
    }

    console.log(steps);
    return solved;
}

exports.autoSolve = autoSolve;

var findBestProblem = function( problems ) {
  var problems = JSON.parse(JSON.stringify(problems));
  var results = [];
  var foundedResults = [];
  var best;
  var bestId = 1000;

  if(problems[0].goal == "max") {
    best = Number.NEGATIVE_INFINITY;
  }
  else {
    best = Number.POSITIVE_INFINITY;
  }

    for(var i = 0; i < problems.length; i++)
    {
        let solved = solve(problems[i]);

        if ((checkIfInts(solved) && solved.goal == "max" && solved.solution.result >= Math.floor(best)) || (solved.status != 'not-important' && solved.solution.feasible && solved.goal == "max" && Math.floor(solved.solution.result) > Math.floor(best)))
        {
          best = solved.solution.result;
          bestId = i;
        }

        if ((checkIfInts(solved) && solved.goal == "min" && solved.solution.result <= Math.ceil(best)) || (solved.status != 'not-important' && solved.solution.feasible && solved.goal == "min" && Math.ceil(solved.solution.result) < Math.ceil(best)))
        {
          best = solved.solution.result;
          bestId = i;
        }

    }

    return bestId;
}

exports.findBestProblem = findBestProblem;

var checkIfInts = function(problem) {
  var problem = Object.assign({}, problem);
  var solved = solve(problem);

  for (var i = 0; i < solved.solution.variables.length ; i++) {
      if(Math.floor(solved.solution.variables[i]) != solved.solution.variables[i])
      {
          return false;
      }
  }

  return true;
}

exports.checkIfInts = checkIfInts;
