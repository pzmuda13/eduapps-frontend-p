var React = require('react');

module.exports = React.createClass({

  render: function() {
    return (
        <div>
          <div className="section-header">
            Informacje
          </div>
          <div className="section-body">
            Zalogowany jako <b>{this.props.user}</b> (grupa <b>{this.props.group}</b>).<br />
            Rozwiązuje zadanie w trybie <b>{this.props.testMode? "testowym" : "nauki"}</b>.
          </div>
        </div>
    );
  }
});
