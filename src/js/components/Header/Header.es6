var React = require('react');
var Switch = require('rc-switch');

module.exports = React.createClass({

  render: function() {
    return (
      <div className="page-header">
            <h2>EduLP <small>Zadanie 1A13BD13ED</small>
              <div className="mode">
                  <div className="label label-default">Tryb {this.props.testMode? 'testowy' : 'nauki'}</div>
                  <div className="switch">
                    <small>Zmień tryb:</small>
                    <Switch onChange={this.props.onModeChange}
                    checked={this.props.testMode}
                    checkedChildren={'T'}
                    unCheckedChildren={'N'}
                    />
                  </div>
              </div>
            </h2>
      </div>
    );
  }
});
