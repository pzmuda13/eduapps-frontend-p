var React = require('react');
var Switch = require('rc-switch');

module.exports = React.createClass({

  render: function() {
    return (
      <div className="dropdown">
        <button className="glyphicon glyphicon-menu-hamburger dropbtn" />
        <div className="dropdown-content">
          <a href="#" onClick={e => {e.preventDefault(); this.props.showUserPanel()}}>Panel użytkownika</a>
          <a href="#" onClick={e => {e.preventDefault(); this.props.logOut()}}>Wyloguj</a>
        </div>
      </div>
    );
  }
});
