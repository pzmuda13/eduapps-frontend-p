import React from 'react';
import Alert from 'react-s-alert';

var $this;

class AlertTemplate extends React.Component {

      constructor(props) {
          super(props);
          $this = this;
      }

      handleClose(change) {
          change && this.props.customFields.handleConfirm();
          $this.props.handleClose();
      }

      handleKeyPress(e) {
        e.keyCode == 13 && $this.handleClose(true);
        e.keyCode == 27 && $this.handleClose(false);
      }

      handleClick(e) {
        var specifiedElement = document.getElementById($this.props.id);

        !specifiedElement.contains(e.target) && $this.handleClose(false);
      }

      componentDidMount() {
        window.addEventListener("keyup", this.handleKeyPress);
        window.addEventListener("mouseup", this.handleClick);
      }

      componentWillUnmount() {
        window.removeEventListener("keyup", this.handleKeyPress);
        window.removeEventListener("mouseup", this.handleClick);
      }

    render() {
      return (
         <div className={this.props.classNames} id={this.props.id} style={this.props.styles}>
             <div className='s-alert-box-inner'>
                 {this.props.message}
             </div>
             <div className={this.props.customFields.confirmation? "alert-buttons": "hidden"}>
             <button className = "button alert-button" onClick={() => {this.handleClose(true)}}>Tak</button>
             <button className = "button alert-button" onClick={() => {this.handleClose(false)}}>Nie</button>
             </div>
         </div>
     )
    }
}

export default AlertTemplate;
