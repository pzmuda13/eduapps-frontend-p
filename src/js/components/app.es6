var React = require('react'),
    SolvingPage = require('./solvingPage'),
    LoginPage = require('./loginPage')

module.exports = React.createClass({

  getInitialState: function() {

     return {
       loggedIn: false,
       component: <LoginPage logIn={this.logIn}/>
     };
  },

  logOut: function(user, group) {
    this.setState({
        loggedIn: false,
        component: <LoginPage logIn={this.logIn}/>
     })
  },

  logIn: function(user, group) {
    this.setState({
       loggedIn: true,
       component: <SolvingPage user={user} group={group} logOut={this.logOut}/>
     })
  },


  render: function() {
    return (
      <div>
      {
        this.state.component
      }
      </div>
    );
  }
});
