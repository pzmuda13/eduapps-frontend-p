var React = require('react');
var Alert = require('react-s-alert').default;
import AlertTemplate from './AlertTemplate';
var Header = require('./Header/Header');
var Info = require('./Info/Info');
var Summary = require('./Summary/Summary');
var Diagram = require('./Diagram/Diagram');
var Solution = require('./Solution/Solution');
var Decisions = require('./Decisions/Decisions');
var Dropdown = require('./Dropdown/Dropdown');
var UserPanel = require('./Userpanel/Userpanel');
var EstimationPanel = require('./Form/EstimationPanel');
var SubproblemsPanel = require('./Form/SubproblemsPanel');

var Solver = require('../ip/problem')

var alertOptions = {
    position: 'top-left',
    effect: 'stackslide',
    html: true,
    timeout: 2000,
    customFields: {}
  };

module.exports = React.createClass({

  getInitialState: function() {

    Solver.init();
    var problem = Solver.newProblem(4,3,4,4);

     return {
       nodes: [ {id: problem.id, label: Solver.generateProblemLabel(problem), level: 0, shape: 'box', color: {background: '#97C2FC', hover: {background: '#D2E5FF'}, highlight: {background: '#D2E5FF'}}} ],
       edges: [],
       problems: [problem],
       activeID: problem.id,
       solution: problem.solution,
       isDisabled: false,
       numOfErrors: 0,
       bestSolution:  Number.POSITIVE_INFINITY,
       solved: false,
       numOfSolved: 0,
       average: 0,
       testMode: false,
       userPanelVisible: false,
       estimationVisible:false,
       subproblemsVisible: false,
       log: []
     };
  },

// Validators
  checkIfNotInts: function(finalSolution) {

    var problem = Object.assign({}, this.state.problems[this.state.activeID-1]);
    var solved = Solver.solve(problem);

    for (var i = 0; i < solved.solution.variables.length ; i++) {

        if(Math.floor(solved.solution.variables[i]) != solved.solution.variables[i])
        {
            if (finalSolution)
            {
              this.logError("Nie wszystkie wspolczynniki są całkowitoliczbowe!")
            }
            return true;
        }
   }

   !finalSolution && this.logError("Wszystkie wspolczynniki są całkowitoliczbowe!");

   return false;
 },

  checkIfBetter: function(shouldBeBetter) {

    var selectedValue = this.state.problems[this.state.activeID-1].solution.result;
    this.findBestSolution();

        if(isFinite(this.state.bestSolution ) && ((this.state.bestSolution >= Math.floor(selectedValue) && this.state.problems[0].goal == 'max') || (this.state.bestSolution <= Math.ceil(selectedValue) && this.state.problems[0].goal == 'min')))
        {
          shouldBeBetter && this.logError("Znaleziono rozwiązanie o takim samym bądź lepszym oszacowaniu!");
          return false;
        }

    return true;
  },
checkIfBest: function() {

        var choosenValue = this.state.problems[this.state.activeID-1].solution.result;
        var bestValue = this.state.problems[Solver.findBestProblem(this.state.problems)].solution.result;

        if( choosenValue != bestValue )
        {
          this.logError("Istnieje problem o lepszym oszacowaniu!");
          return false;
        }

    return true;
  },

  checkIfEstimated: function(shouldBeEstimated) {
    if(this.state.problems[this.state.activeID-1].estimate)
    {
      !shouldBeEstimated && this.logError("Już oszacowałeś wartość tego podproblemu!");
      return true;
    }
      shouldBeEstimated && this.logError("Oszacuj najpierw wartosc funkcji!");
    return false;
  },

  checkIfSolved: function(shouldBeSolved) {
    if(this.state.problems[this.state.activeID-1].solution)
    {
        !shouldBeSolved && this.logError("Rozwiązałeś już problem RL!");

      return true;
    }
      shouldBeSolved &&  this.logError("Rozwiąż problem RL!");
    return false;
  },
//end

//Handling state

  findBestSolution: function() {

    var problems = JSON.parse(JSON.stringify(this.state.problems));
    var results = [];
    var foundedResults = [];
    var bestFound;
    var index;

      for(var i = problems.length-1; i >= 0; --i)
      {
          let solved = Solver.solve(problems[i]);

          problems[i].solution && problems[i].solution.feasible && problems[i].status != 'not-important' && Solver.checkIfInts(solved) && foundedResults.push(problems[i].solution.result);

      }

        if(problems[0].goal == "max") {
          bestFound = Math.max.apply(null, foundedResults);
        }
        else {
          bestFound = Math.min.apply(null, foundedResults);
        }

        this.setState({
          bestSolution: bestFound
        });
  },

  selectedNode: function(number) {
    if(parseInt(number) != this.state.activeID)
    {
      this.state.activeID = parseInt(number);
      this.getSelectedProblemSolution();
      this.activeIdIsDisabled();
    }
  },

  getSelectedProblemSolution: function() {
    if(!isNaN(this.state.activeID) && this.state.problems[this.state.activeID-1].solution)
    {
      this.setState({
          solution: this.state.problems[this.state.activeID-1].solution
      });
    }
    else {
      this.setState({
          solution: null
      });
    }
  },
  activeIdIsDisabled: function() {
    let status = false;

    if(!isNaN(this.state.activeID))
    {
      if(this.state.problems[this.state.activeID-1].status == 'not-important')
      {
        status = true;
      }
    }
    else
    {
      status = true;
    }

    this.setState({
        isDisabled: status,
        estimationVisible: false,
        subproblemsVisible: false
    });
  },

handleChangeMode: function() {
  this.setState({
    testMode: !this.state.testMode
  });
        this.restart();
},

 onModeChange: function() {
   let options = Object.assign({},alertOptions);
   options.timeout = 'none';
   options.customFields = {
         confirmation: true,
         handleConfirm: this.handleChangeMode
     }

      Alert.warning('<div class="alertMessage">Czy na pewno chcesz zmienić tryb?</div>', options);
 },

   updateNodes: function(nodes? = this.state.nodes) {
     var newNodes = JSON.parse(JSON.stringify(nodes));
     for (var i=0; i < this.state.problems.length; i++)
     {
       if (Solver.checkIfInts(this.state.problems[i]) && this.state.problems[i].solution && this.state.problems[i].solution.feasible)
       {
         this.state.problems[i].estimate = this.state.problems[i].solution.result;
         newNodes[i].label = Solver.generateProblemLabel(this.state.problems[i], this.state.problems[i].estimate);
         newNodes[i].color.background ='#27ae60';
         newNodes[i].color.hover.background ='#29db5b';
         newNodes[i].color.highlight.background ='#29db5b';
       }
       if (this.state.problems[i].status == "not-important" || (this.state.problems[i].solution && !this.state.problems[i].solution.feasible))
       {
         var estimate;
         !this.state.problems[i].estimate? estimate = "Brak" : estimate=this.state.problems[i].estimate;
         newNodes[i].label = Solver.generateProblemLabel(this.state.problems[i], estimate);
         newNodes[i].color.background ='#b2b2b2';
         newNodes[i].color.hover.background ='#d7d9dd';
         newNodes[i].color.highlight.background ='#d7d9dd';
       }
     }
     this.setState ({
       nodes: newNodes
     });
   },

     showUserPanel: function() {
       this.setState({
           userPanelVisible: true
        })
     },

     hideUserPanel: function() {
       this.setState({
           userPanelVisible: false
        })
     },

     logError: function(error) {
       Alert.error(`<div class="alertMessage">${error}</div>`, alertOptions);
       this.state.log.push(`blad: ${error}`);
       console.log(`blad: ${error}`);
       this.setState({
           numOfErrors: this.state.numOfErrors+1
       });
     },

     logDecision: function(decision) {
       this.state.log.push(`probelm id: ${this.state.activeID} decyzja: ${decision}`);
       console.log(`probelm id: ${this.state.activeID} decyzja: ${decision}`);
     },

       restart: function() {

         Solver.init();
         var problem = Solver.newProblem(4,3,4,4);

         this.setState({
            nodes: [ {id: problem.id, label: Solver.generateProblemLabel(problem), level: 0, shape: 'box', color: {background: '#97C2FC', hover: {background: '#D2E5FF'}, highlight: {background: '#D2E5FF'}}} ],
            edges: [],
            problems: [problem],
            activeID: problem.id,
            solution: problem.solution,
            isDisabled: false,
            numOfErrors: 0,
            solved: false,
            bestSolution: Number.POSITIVE_INFINITY,
            bestValue: Number.POSITIVE_INFINITY,
            estimationVisible: false,
            subproblemsVisible: false
          })
       },
 //end

 //Actions

  addSubproblems: function(constraintsInput) {
    this.logDecision("wprowadzono nowe ograniczenia");
            var newConstraints = Solver.createSubproblems(this.state.problems[this.state.activeID-1],true).constraints;

            if(JSON.stringify(constraintsInput) === JSON.stringify(newConstraints))
            {
              var newProblems = Solver.createSubproblems(this.state.problems[this.state.activeID-1],false).problems;
              var newNodes = [];
              var newEdges = [];

              for(var i = 0; i < newProblems.length; i++)
              {
                newNodes.push({id: newProblems[i].id, label: Solver.generateProblemLabel(newProblems[i]), level: this.state.nodes[this.state.activeID-1].level+1, shape: 'box', color: {background: '#97C2FC', hover: {background: '#D2E5FF'}, highlight: {background: '#D2E5FF'}}});
                newEdges.push({from: newProblems[i].parent_id, to: newProblems[i].id, arrows: 'to'});
              }

              this.state.problems = this.state.problems.concat(newProblems);

              this.state.nodes = this.state.nodes.concat(newNodes);
              this.state.edges = this.state.edges.concat(newEdges);
              this.state.problems[this.state.activeID-1].status = 'not-important';
              this.state.activeID = newProblems[0].id;
              this.getSelectedProblemSolution();

              this.setState({
                            isDisabled: false,
                            subproblemsVisible: false
              });
          }
          else {
            this.logError('Wprowadzone ograniczenia nie są prawidłowe!');
            this.setState({
                numOfErrors: this.state.numOfErrors+1
            });
          }
          this.findBestSolution();
          this.updateNodes();
  },

  subproblems: function() {
    this.logDecision("dodaj podproblemy");
    if(!isNaN(this.state.activeID) && !this.state.isDisabled)
    {
       if (this.checkIfSolved(true) && this.checkIfNotInts(false) && this.checkIfEstimated(true) && this.checkIfBetter(true))
       {
         this.setState({
             subproblemsVisible: true
         })
       }
    }
  },

  addEstimation: function(estimation) {
    this.logDecision("wprowadz oszacowanie");
    var answer;

    this.state.problems[this.state.activeID-1].goal == "max"? answer=Math.floor(this.state.problems[this.state.activeID-1].solution.result) : answer=Math.ceil(this.state.problems[this.state.activeID-1].solution.result);

    if(new Number(estimation) == answer)
    {
      var newNodes = JSON.parse(JSON.stringify(this.state.nodes));
      newNodes[this.state.activeID-1].label = Solver.generateProblemLabel(this.state.problems[this.state.activeID-1], estimation);

      this.state.problems[this.state.activeID-1].estimate = estimation;

      this.setState({
          nodes: newNodes,
          estimationVisible: false
      })
    }
    else {
      this.logError('Wprowadź poprawne oszacowanie!');
      this.setState({
          numOfErrors: this.state.numOfErrors+1
      });
    }
  },

  estimate: function() {
    this.logDecision("oszacuj");
    if(!isNaN(this.state.activeID) && !this.state.isDisabled)
    {
       if (this.checkIfSolved(true) && this.checkIfNotInts(false) && !this.checkIfEstimated(false))
       {
         this.setState({
             estimationVisible: true
         })
       }
    }
  },

  solveProblem: function() {
    this.logDecision("rozwiaz RL");
      if(!isNaN(this.state.activeID) && !this.state.isDisabled && !this.checkIfSolved(false))
      {
        this.state.problems[this.state.activeID-1] = Solver.solve(this.state.problems[this.state.activeID-1]);
        if(!this.state.problems[this.state.activeID-1].solution.feasible) this.state.problems[this.state.activeID-1].status="not-important";
        var newNodes = JSON.parse(JSON.stringify(this.state.nodes));
        newNodes[this.state.activeID-1].label = Solver.generateProblemLabel(this.state.problems[this.state.activeID-1], null);

        this.getSelectedProblemSolution();
        this.activeIdIsDisabled();
        this.findBestSolution();

        this.updateNodes(newNodes);
      }
  },


    checkIfSolution: function() {
      this.logDecision('To jest rozwiązanie')
      if( !this.state.isDisabled && !isNaN(this.state.activeID))
      {
            if( this.checkIfEstimated(true) && !this.checkIfNotInts(true) && this.checkIfBest() )
           {
             Alert.success('<div class="alertMessage">Gratulacje, to prawidłowe rozwiązanie!</div>', alertOptions);
             this.state.log.push("rozwiązywanie zakończone");
             this.state.log.push(this.state.problems)
             console.log(this.state.log);

             var average = (this.state.average*this.state.numOfSolved+this.state.numOfErrors)/(this.state.numOfSolved+1);

             this.setState({
                 solved: true,
                 numOfSolved: this.state.numOfSolved+1,
                 average: average
             });
           }
           else {
             this.setState({
                 numOfErrors: this.state.numOfErrors+1
             });
           }
        }
   },

disableProblem: function() {
  if(!this.state.isDisabled && !isNaN(this.state.activeID) && this.checkIfSolved(true) && !this.checkIfBetter(false))
  {
    this.state.problems[this.state.activeID-1].status = "not-important";
    this.updateNodes();
  }
  else {
    this.logError("Nie można skreślić tego problemu!")
  }
},
//end

  render: function() {
    return (
      <div>
      <Dropdown logOut={this.props.logOut} showUserPanel={this.showUserPanel}/>
      <div className="flexBox">
            <UserPanel visible={this.state.userPanelVisible} hide={this.hideUserPanel} user={this.props.user} group={this.props.group} solved={this.state.numOfSolved} average={this.state.average}/>
            <Alert contentTemplate={AlertTemplate} stack={{limit: 1}} />
          <Diagram nodes={this.state.nodes} edges={this.state.edges} selectedNode={this.selectedNode} activeID={this.state.activeID} numOfVariables={this.state.problems[0].performance_index.length} numOfCons={this.state.problems[this.state.problems.length-1].constraints.length}/>
        <div id="menu" className="menu">
          <Header testMode={this.state.testMode} onModeChange={this.onModeChange}/>
          <Info user={this.props.user} group={this.props.group} testMode={this.state.testMode}/>
          <Summary goal={this.state.problems[0].goal} bestFoundSolution={this.state.bestSolution} numOfErrors={this.state.numOfErrors}/>
          <Solution selectedProblemSolution={this.state.solution}/>
          {this.state.estimationVisible && <EstimationPanel addEstimation={(estimation) => this.addEstimation(estimation)} />}
          {this.state.subproblemsVisible && <SubproblemsPanel addSubproblems={(constraints) => this.addSubproblems(constraints)} numOfVariables={this.state.problems[0].performance_index.length}/>}
          <Decisions showSubproblems={this.subproblems} disableProblem={this.disableProblem} isDisabled={this.state.isDisabled} solveProblem={this.solveProblem} checkIfSolution={this.checkIfSolution} isSolved={this.state.solved} restart={this.restart} showEstimation={this.estimate}/>
        </div>
      </div>
      </div>
    );
  }
});
