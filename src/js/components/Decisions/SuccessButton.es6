var React = require('react');
var classNames = require('classnames')

module.exports = React.createClass({

  render: function() {
    return (
      <div
          onClick={e => {
              e.preventDefault();
              this.props.onClick();
          } }
          className= { classNames("success",  {"hidden": !this.props.isVisible})}
          >
          Rozwiąż kolejny przykład
      </div>
    );
  }
});
