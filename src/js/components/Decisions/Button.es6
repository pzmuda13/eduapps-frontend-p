var React = require('react');
var classNames = require('classnames')

module.exports = React.createClass({

  render: function() {
    return (
      <div
          onClick={e => {
              e.preventDefault();
              this.props.onClick();
          } }
          className= { classNames("decision", {"decision-disabled": this.props.isDisabled}, {"hidden": !this.props.isVisible})}
          >
          {this.props.description}
      </div>
    );
  }
});
