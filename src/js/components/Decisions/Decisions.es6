var React = require('react');
var $Button = require('./Button');
var SuccessButton = require('./SuccessButton');

module.exports = React.createClass({

  render: function() {
    return (
       <div>
         <div className="section-header">
           Decyzje
         </div>
         <div className="decisions-container">
            <$Button isDisabled={this.props.isDisabled} isVisible={!this.props.isSolved} description = "Rozwiąż problem RL." onClick={this.props.solveProblem}/>
            <$Button isDisabled={this.props.isDisabled} isVisible={!this.props.isSolved} description = "Wprowadź oszacowanie problemu." onClick={this.props.showEstimation}/>
            <$Button isDisabled={this.props.isDisabled} isVisible={!this.props.isSolved} description = "Rozbij problem na podproblemy." onClick={this.props.showSubproblems}/>
            <$Button isDisabled={this.props.isDisabled} isVisible={!this.props.isSolved} description = "Skreśl problem." onClick={this.props.disableProblem}/>
            <$Button isDisabled={this.props.isDisabled} isVisible={!this.props.isSolved} description = "To jest ostateczne rozwiązanie." onClick={this.props.checkIfSolution}/>
            <$Button isDisabled={false} description = "Rozwiąż inny przykład." isVisible={true} onClick={this.props.restart}/>
         </div>
       </div>
    );
  }
});
