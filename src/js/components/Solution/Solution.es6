var React = require('react');

module.exports = React.createClass({


  solution: function() {

    var solution = <div>Brak rozwiązania (wybierz podproblem bądź oszacuj jego wartość)</div>;

    if(this.props.selectedProblemSolution)
    {
        solution = [];
        var obj = this.props.selectedProblemSolution;
        if(obj.feasible)
        {
          for (var i = 0; i < obj.variables.length; i++) {
            solution.push(<div key={i}>x{i+1} = <b>{obj.variables[i]}</b></div>);
          }

          solution.push(<div>Wynik = <b>{obj.result}</b></div>);
        }
        else {
          solution.push(<b>Wybrany podproblem nie ma rozwiązań.</b>)
        }
    }

    return (
      <div id="solution">
          {solution}
      </div>
    );
  },

  shouldComponentUpdate: function(nextProps, nextState) {
  return nextProps.selectedProblemSolution !== this.props.selectedProblemSolution;
  },

  componentDidUpdate: function() {
  var scroll = document.getElementById('menu');
  scroll.scrollTop = scroll.scrollHeight;
  },

  render: function() {

    return (
        <div>
          <div className="section-header">
            Rozwiązanie aktualnie wybranego problemu
          </div>
          <div className="section-body">
            {this.solution()}
          </div>
        </div>
    );
  }
});
