var React = require('react');
var Alert = require('react-s-alert').default;

var alertOptions = {
    position: 'top',
    effect: 'stackslide',
    html: true,
    timeout: 1500
  };


var bans = [
  {
    reason: "logowanie na konta z tego samego ip",
    date: "20.10.2016"
  },
  {
    reason: "logowanie na konta z tego samego ip",
    date: "20.10.2016"
  }
]


module.exports = React.createClass({

  getInitialState: function() {

     return {
     };
  },

  handleKeyPress: function(e) {
    e.keyCode == 27 && this.props.hide();
  },

  handleClick: function(e) {
    var specifiedElement = document.getElementById('panel');

    !specifiedElement.contains(e.target) && this.props.hide();
  },

  getBans: function() {
    return bans.map(ban =>
                <div className="ban"><span>{ban.reason}</span><span className="date">{ban.date}</span></div>
            )
  },

  componentDidMount: function() {
    window.addEventListener("keyup", this.handleKeyPress);
    window.addEventListener("mouseup", this.handleClick);
  },

  componentWillUnmount: function() {
    window.removeEventListener("keyup", this.handleKeyPress);
    window.removeEventListener("mouseup", this.handleClick);
  },

  render: function() {
    return (
      <div className={this.props.visible? "userPanelContainer": "hidden"}>
        <div id="panel" className="userPanel" onBlur={this.props.hide}>
          <div className="userPanel-header">
            <i className="glyphicon glyphicon-user avatar"></i>
            <div className="username">{this.props.user},<br />grupa {this.props.group}</div>
            <button className="glyphicon glyphicon-remove closeButton" onClick={this.props.hide} />
          </div>
          <div className="userPanel-statistics">
            Statystyki
              <div className="stat">Liczba rozwiązanych zadań <span className="number">{this.props.solved}</span> </div>
              <div className="stat">Średnia ilość błędów <span className="number">{this.props.average.toFixed(2)}</span> </div>
          </div>
          <div className="userPanel-bans">
            Lista banów
            <div className="ban"><span>Powód</span><span className="date">data</span></div>
              {this.getBans()}
          </div>
        </div>
      </div>
    );
  }
});
