var React = require('react');

module.exports = React.createClass({

  handleKeyPress(e) {
    e.keyCode == 13 && this.props.addEstimation(this.refs.estimation.value);
  },

  componentDidMount() {
    var scroll = document.getElementById('menu');
    scroll.scrollTop = scroll.scrollHeight;
    document.getElementById('estimation').focus();
    window.addEventListener("keyup", this.handleKeyPress);
  },

  componentWillUnmount() {
    window.removeEventListener("keyup", this.handleKeyPress);
  },

  render: function() {

    return (
        <div>
          <div className="section-header">
            Wprowadź oszacowanie
          </div>
          <div className="section-body">
            <input id="estimation" ref="estimation" type="number" className="input-form"/>
            <button type="button" className="button-form" onClick={() => this.props.addEstimation(this.refs.estimation.value)}>Dodaj</button>
          </div>
        </div>
    );
  }
});
