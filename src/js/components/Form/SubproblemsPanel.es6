var React = require('react');

module.exports = React.createClass({

  handleKeyPress(e) {
    e.keyCode == 13 && this.props.addSubproblems(this.getConstraints(this.props.numOfVariables));
  },

  componentDidMount() {
    var scroll = document.getElementById('menu');
    scroll.scrollTop = scroll.scrollHeight;
    document.getElementById('x1 down').focus();
    window.addEventListener("keyup", this.handleKeyPress);
  },

  componentWillUnmount() {
    window.removeEventListener("keyup", this.handleKeyPress);
  },

  generateVariables(numOfVariables) {
    var form = [];
    for (var i = 0; i < numOfVariables; i++) {
      form.push(<div className="constraints"><input id={`x${i+1} down`} ref={`x${i+1} down`} type="number" className="input-form"/><b> >= </b>{`x${i+1}`}<b> >= </b><input id={`x${i+1} up`} ref={`x${i+1} up`} type="number" className="input-form"/></div>);
    }

    return form;
  },

  getConstraints(numOfVariables) {
    var newConstraints = [];
    for (var i = 0; i < numOfVariables; i++) {
      if(document.getElementById(`x${i+1} down`).value && document.getElementById(`x${i+1} up`).value)
      {
        var constraint1 = new Array(numOfVariables).fill(0);
        var constraint2 = new Array(numOfVariables).fill(0);

        constraint1[i] = 1;
        constraint2[i] = 1;

        newConstraints.push(constraint1.concat(['<=',new Number(document.getElementById(`x${i+1} down`).value)]));
        newConstraints.push(constraint2.concat(['>=',new Number(document.getElementById(`x${i+1} up`).value)]));
      }
    }

    return newConstraints;
  },

  render: function() {

    return (
        <div>
          <div className="section-header">
            Wprowadź nowe ograniczenia - zostaw puste gdy nie dotyczy danej zmiennej
          </div>
          <div className="section-body">
            <div className="subproblems-form">
              {this.generateVariables(this.props.numOfVariables)}
              <button type="button" className="button-form" onClick={() => this.props.addSubproblems(this.getConstraints(this.props.numOfVariables))}>Wprowadź</button>
            </div>
          </div>
        </div>
    );
  }
});
