var React = require('react');

module.exports = React.createClass({

  render: function() {
    return (
        <div>
          <div className="section-header">
            Podsumowanie
          </div>
          <div className="section-body">
            Cel optymalizacji: <b>{this.props.goal}</b><br />
            Najlepsze znalezione rozwiązanie: <b>{isFinite(this.props.bestFoundSolution )? this.props.bestFoundSolution.toFixed(2) : "brak"}</b><br />
            Liczba błędów w tym przykładzie: <b>{this.props.numOfErrors}</b>
          </div>
        </div>
    );
  }
});
