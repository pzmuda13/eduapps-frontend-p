var React = require('react');
var Vis = require('vis');

var network;

var options = {
    autoResize: true,
    height: '100%',
    physics: {
                enabled: false
             },
    layout: {
        hierarchical: {
            direction: "UD",
            parentCentralization: false
        }
    },
    interaction: {dragNodes :false, hover: true, selectConnectedEdges: false},
    nodes: {shadow: true},
    edges: {shadow: true}
};

module.exports = React.createClass({

  onClick: function() {
    this.props.selectedNode(network.getSelectedNodes());
  },

  drawDiagram: function() {
    var container = document.getElementById('net-problem');

    var data = {
      nodes: new Vis.DataSet(this.props.nodes),
      edges: new Vis.DataSet(this.props.edges)
    };

    network = new Vis.Network(container, data, options);
    network.selectNodes([this.props.activeID],true);
    network.on('click', this.onClick);
  },

  componentDidMount: function() {
      this.drawDiagram();
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    for (var i= 0; i < nextProps.nodes.length; i++)
    {
       if(JSON.stringify(nextProps.nodes[i]) !== JSON.stringify(this.props.nodes[i]))
       {
         return true;
       }
    }
    return false;
  },

  componentDidUpdate: function() {


      options.layout.hierarchical.levelSeparation = 100+15*this.props.numOfCons;
      options.layout.hierarchical.nodeSpacing = 55*this.props.numOfVariables;

      network.setOptions(options);

      network.setData({
        nodes: new Vis.DataSet(this.props.nodes),
        edges: new Vis.DataSet(this.props.edges)
      });
      if(this.props.activeID)
      {
        network.selectNodes([this.props.activeID],true);
      }
  },

  render: function() {

    return (
        <div className="diagramContainer">

              <div className="diagramLabel">Graf rozwiązania</div>
              <div id="net-problem" className="diagram">
            </div>
        </div>
    );
  }
});
