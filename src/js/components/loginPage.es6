var React = require('react');
var Alert = require('react-s-alert').default;

var alertOptions = {
    position: 'top',
    effect: 'stackslide',
    html: true,
    timeout: 1500
  };

module.exports = React.createClass({

  getInitialState: function() {

     return {
        usr: 'Guest',
        group: '1'
     };
  },

  handleUsrChange: function(e) {
    this.setState({usr: e.target.value});
  },

  handleGroupChange: function(e) {
    this.setState({group: e.target.value});
  },

  handleKeyPress: function(e) {
    e.keyCode == 13 && this.login();
  },

  login: function() {
    (this.state.usr.length && this.state.group.length)? this.props.logIn(this.state.usr, this.state.group) :  Alert.error('<div class="alertMessage">Wprowadź swoje dane!</div>', {
                                                                                                                           position: 'top',
                                                                                                                           effect: 'stackslide',
                                                                                                                           html: true
                                                                                                                         });
  },

  componentDidMount: function() {
    window.addEventListener("keyup", this.handleKeyPress);
  },

  componentWillUnmount: function() {
    window.removeEventListener("keyup", this.handleKeyPress);
  },

  render: function() {
    return (
      <div className="login-page">
          <Alert stack={{limit: 1}} />
          <div className="login-circle center">
              <div className="user center mb5">
                  <i className="glyphicon glyphicon-user icon-position"></i>
              </div>
              <div className="form">
                <input id="usr" type="text" value={this.state.usr} placeholder="Imię i Nazwisko" className="input" onChange={this.handleUsrChange} />
                <input id="group" type="text" value={this.state.group} placeholder="Numer grupy" className="input" onChange={this.handleGroupChange} />
                <button type="button" className="button logIn" onClick={this.login}>login</button>
              </div>
          </div>
      </div>
    );
  }
});
