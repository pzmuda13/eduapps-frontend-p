var should = require('should'),
    ip     = require('../../../src/js/ip/problem');

describe('IP Problem', () => {

  beforeEach( () => {
    ip.init();
  });

  it( 'create a brand new IP problem', () => {
    let p = ip.newProblem({
      performance_index: [1, 2],
      goal: 'min',
      constraints: [
        [1, 0, 'ge', 5],
        [0, 1, 'ge', 3]
      ]
    });

    p.should.deepEqual({
      id: 1,
      parent_id: null,
      performance_index: [1, 2],
      goal: 'min',
      constraints: [
        [1, 0, 'ge', 5],
        [0, 1, 'ge', 3]
      ],
      solution: null,
      estimate: null,
      status: 'to-solve'
    });
  });

  it( 'solve an LP problem', () => {
    let p = ip.newProblem({
      performance_index: [1, 2],
      goal: 'min',
      constraints: [
        [1, 0, 'ge', 5],
        [0, 1, 'ge', 3]
      ]
    });

    let solved_p = ip.solve(p);

    solved_p.should.deepEqual({
      id: 1,
      parent_id: null,
      performance_index: [1, 2],
      goal: 'min',
      constraints: [
        [1, 0, 'ge', 5],
        [0, 1, 'ge', 3]
      ],
      solution: {
        value: 11,
        variables: [5, 3]
      },
      estimate: null,
      status: 'to-solve'
    });
  });

});
